package com.techxora.taco;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostResponseModel {
    @Expose
    @SerializedName("Status")
    private String Status;
    @Expose
    @SerializedName("Time")
    private String Time;
    @Expose
    @SerializedName("Longitude")
    private double Longitude;
    @Expose
    @SerializedName("Latitude")
    private double Latitude;
    @Expose
    @SerializedName("Accurracy")
    private double Accurracy;
    @Expose
    @SerializedName("NetworkOrGps")
    private String NetworkOrGps;
    @Expose
    @SerializedName("UserID")
    private String UserID;
}
