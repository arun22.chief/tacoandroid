package com.techxora.taco;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import org.json.JSONObject;

public class WebAppInterface {
    Context mContext;
    LocationSend locationSend;

    WebAppInterface(Context context) {
        mContext = context;
        locationSend = (LocationSend) mContext;
    }


    public interface LocationSend {
        void startLocation(String UserID, int frequency, String networkType);

        void stopLocation();

        JSONObject currentLocation();
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public JSONObject GetCurrentLocationInfo() {
       return locationSend.currentLocation();
    }

    @JavascriptInterface
    public void StartLocation(String UserID, int frequency, String networkType) {
        locationSend.startLocation(UserID, frequency,networkType);
    }

    @JavascriptInterface
    public void StopLocation() {
        locationSend.stopLocation();
    }
}
