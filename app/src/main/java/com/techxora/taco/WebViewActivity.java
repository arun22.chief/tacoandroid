package com.techxora.taco;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Calendar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebViewActivity extends AppCompatActivity implements WebAppInterface.LocationSend {
    private Handler mHandler;
    private String mUserId = "";
    private int mFrequency = 0;
    private String mNetworkType = "GPS";
    protected LocationManager locationManager;
    Location mLocation;
    private LocationListener locListener;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        WebView webView = findViewById(R.id.wv);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        webView.loadUrl(AppConstants.BaseUrl);
        WebSettings wvSettings = webView.getSettings();
        wvSettings.setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new WebAppInterface(this), "Android");
        mHandler = new Handler();
    }

    @Override
    public void startLocation(String UserID, int frequency, String networkType) {
        if (networkType != null) {
            mNetworkType = networkType;
        }
        if (UserID != null && frequency != 0) {
            mUserId = UserID;
            mFrequency = frequency;
            startRepeatingTask();
        }
    }

    @Override
    public void stopLocation() {
        stopRepeatingTask();
    }

    @Override
    public JSONObject currentLocation() {
        mNetworkType = "GPS_OR_NETWORK";
        getLastKnownLocation();
        JSONObject result = new JSONObject();
        try {
            if (mLocation != null) {
                result.put("Latitude", mLocation.getLatitude());
                result.put("Longitude", mLocation.getLongitude());
            }
            result.put("Network Type", mNetworkType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    void startRepeatingTask() {
        getLastKnownLocation();
        if (mLocation != null && !mNetworkType.equals("GPS"))
            mStatusChecker.run();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                callLocationDetailsPostApi();
            } finally {
                mHandler.postDelayed(mStatusChecker, mFrequency);
            }
        }
    };

    private void getLastKnownLocation() {
        if (locationManager == null)
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (mNetworkType.equals("GPS")) {
            locListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mLocation = location;
                    callLocationDetailsPostApi();
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            };
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.
                            permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.INTERNET
                    }, MY_PERMISSIONS_REQUEST_LOCATION);
                    return;
                } else {
                    configureButton();
                }
            } else {
                configureButton();
            }
        } else {
            mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (mLocation == null) {
                mLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    configureButton();
                return;
        }
    }

    private void configureButton() {
        locationManager.requestLocationUpdates("gps", mFrequency, 10, locListener);
    }

    private void callLocationDetailsPostApi() {
        GetDataInterface getDataInterface = RetrofitClientInstance.getRetrofitInstance().create(GetDataInterface.class);
        Call<PostResponseModel> call = getDataInterface.postLocation(mUserId, mNetworkType, 100, mLocation.getLatitude(), mLocation.getLongitude(), String.valueOf(Calendar.getInstance().getTime()));
        call.enqueue(new Callback<PostResponseModel>() {
            @Override
            public void onResponse(Call<PostResponseModel> call, Response<PostResponseModel> response) {
                Gson gson = new Gson();
                String json = gson.toJson(response.body());
                Toast.makeText(WebViewActivity.this, "Location sent Successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<PostResponseModel> call, Throwable t) {
                Toast.makeText(WebViewActivity.this, "Location sent failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void stopRepeatingTask() {
        mUserId = "";
        mFrequency = 0;
        mHandler.removeCallbacks(mStatusChecker);
        locationManager.removeUpdates(locListener);
    }

}
