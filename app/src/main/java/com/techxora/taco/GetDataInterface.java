package com.techxora.taco;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GetDataInterface {

    @POST("Home/PostLocation")
    Call<PostResponseModel> postLocation(@Query("UserID") String userID, @Query("NetworkOrGps") String network,
                                         @Query("Accurracy") int accuracy, @Query("Latitude") double latitude,
                                         @Query("Longitude") double longitude, @Query("Time") String time);
}
